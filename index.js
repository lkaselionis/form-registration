const path = require('path')
const express = require('express')
const app = express()
app.use(express.static('public'))
const port = 3000

const url = require('url')

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// comment
app.get('/', (req, res) => res.sendFile('index.html', { root: __dirname }))
app.get('/register', (req, res) => {
    let url_parts = url.parse(req.url, true)

    console.log(req.body)
    res.sendFile(path.join(__dirname, 'public/thankyou.html'))
})

app.post('/register', (req, res) => {
    console.log(req.body)
    res.sendFile(path.join(__dirname, 'public/thankyou.html'))
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))